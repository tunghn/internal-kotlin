package com.example.appdemo.ViewModel

import android.widget.Toast
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class HomeViewModel : ViewModel() {
    private var listNumber : MutableLiveData<MutableList<String>> = MutableLiveData()
    var list = ArrayList<String>()

    @JvmName("getTextNumber1")
    fun getTextNumber(): MutableLiveData<MutableList<String>> {
        return listNumber
    }

    fun setTextNumber(number: String) {
        list.add(number)
        listNumber.value = list
    }
}