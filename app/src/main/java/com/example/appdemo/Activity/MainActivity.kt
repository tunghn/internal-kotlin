package com.example.appdemo.Activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.appdemo.R
import com.example.appdemo.databinding.ActivityMainBinding
import com.example.appdemo.fragment.HomeFragment

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        supportFragmentManager.beginTransaction().replace(R.id.fragment_container_view_tag,HomeFragment()).commit()
    }
}