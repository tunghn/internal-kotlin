package com.example.appdemo.Activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.appdemo.R
import com.example.appdemo.databinding.ActivityLoginBinding

class LoginActivity : AppCompatActivity() {
    private lateinit var binding: ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.webView.loadUrl("file:///android_asset/background.html")
    }
}