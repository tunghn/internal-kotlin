package com.example.appdemo.Canvas

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View


class DrawLine(context: Context?) : View(context) {
    var paint = Paint()
    private fun init() {
        paint.color = Color.BLACK
    }

    public override fun onDraw(canvas: Canvas) {
        canvas.drawLine(10f, 10f, 20f, 20f, paint)
    }
}