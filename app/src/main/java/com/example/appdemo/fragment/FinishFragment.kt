package com.example.appdemo.fragment

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentResultListener
import com.example.appdemo.R
import com.example.appdemo.databinding.FragmentFinishBinding

class FinishFragment : Fragment() {

    private lateinit var _binding : FragmentFinishBinding
    private val binding get() = _binding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding =  FragmentFinishBinding.inflate(layoutInflater)
        parentFragmentManager.setFragmentResultListener("data",this,object : FragmentResultListener {
            override fun onFragmentResult(requestKey: String, result: Bundle) {
                val account = result.getString("account")
                val price = result.getString("price")
                binding.tvAccount.text = account
                binding.tvPrice.text = price
            }

        })
        return binding.root
    }
}