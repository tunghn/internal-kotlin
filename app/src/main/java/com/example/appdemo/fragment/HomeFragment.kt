package com.example.appdemo.fragment

import android.graphics.Paint
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.FragmentResultListener
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.appdemo.Adapter.ListAccountNumberAdapter
import com.example.appdemo.R
import com.example.appdemo.ViewModel.HomeViewModel
import com.example.appdemo.databinding.FragmentHomeBinding
import java.util.*
import kotlin.collections.ArrayList

class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!
    private lateinit var viewModel: HomeViewModel

    private var characters = 4
    private lateinit var mAdapter: ListAccountNumberAdapter
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeBinding.inflate(layoutInflater)
        viewModel = ViewModelProvider(requireActivity()).get(HomeViewModel::class.java)


        setUpSpinner()
        binding.tvKiemTra.setOnClickListener {
            btnKiemTra()
        }
        btnDangKy()
        return binding.root
    }

    private fun btnDangKy() = binding.btnDangKy.apply {
        setOnClickListener {
            val account = mAdapter.getAccountChoose()
            val price = mAdapter.getPrice()
            requireActivity().supportFragmentManager.beginTransaction()
                .replace(R.id.fragment_container_view_tag, FinishFragment()).commit()
            val bundle = Bundle()
            bundle.putString("account", account)
            bundle.putString("price",price)
            parentFragmentManager.setFragmentResult("data", bundle)
        }
    }

    private fun setUpSpinner() = binding.apply {
        tvKiemTra.paintFlags = tvKiemTra.paintFlags or Paint.UNDERLINE_TEXT_FLAG
        val options: ArrayList<String> = ArrayList()
        for (i in 4..10) {
            options.add("$i số cuối")
        }

        autoComplete.setAdapter(
            ArrayAdapter(
                requireContext(),
                android.R.layout.simple_list_item_1,
                options
            )
        )

        autoComplete.setOnItemClickListener { adapterView, view, i, l ->
            run {
                characters = options[i].substring(0, 2).trim().toInt()
                setMaxNumberOtp(characters)
            }
        }
    }

    private fun setMaxNumberOtp(number: Int) = binding.edtNumber.apply {
        mMaxLength = number
        mNumChars = number.toFloat()
        Log.e("TAG", "setMaxNumberOtp: $number")
        invalidate()
    }


    private fun btnKiemTra() = binding.apply {
        val textNumber = edtNumber.text.toString()
        if (textNumber.length != characters) {
            Toast.makeText(requireContext(), "Bạn phải nhập đủ số", Toast.LENGTH_SHORT).show()
            return@apply
        }
        setListNumber(textNumber)
        setUpRecyclerView()
    }

    private fun setUpRecyclerView() = binding.rcvAccount.apply {
        binding.tvKiemTra.visibility = View.GONE
        binding.tvTitle.visibility = View.VISIBLE
        visibility = View.VISIBLE
        viewModel.getTextNumber().observe(viewLifecycleOwner, Observer {
            mAdapter = ListAccountNumberAdapter(it)
        })
        adapter = mAdapter
        layoutManager = LinearLayoutManager(requireContext())
    }

    private fun setListNumber(textNumber: String) {
        for (j in 0..10) {
            var textFull = ""
            var stk = ""
            for (i in 0..15 - textNumber.length) {
                textFull += Random().nextInt(9).toString()
            }
            textFull += textNumber
            for (i in 0..textFull.length) {
                if ((i + 1) % 4 == 0) {
                    stk += textFull.substring(i - 3, i + 1) + " "
                }
            }
            viewModel.setTextNumber(stk)
        }
    }


}