package com.example.appdemo.Adapter

import android.annotation.SuppressLint
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.appdemo.databinding.ItemListAccountNumberBinding

class ListAccountNumberAdapter(val list: List<String>) : RecyclerView.Adapter<ListAccountNumberAdapter.ViewHolder>() {
    class ViewHolder(val binding: ItemListAccountNumberBinding) : RecyclerView.ViewHolder(binding.root){
        fun bind(item : String) = binding.apply{
            tvAccount.text = item
            tvPrice.text = "Phí DV : 80,000,000 VND"
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ItemListAccountNumberBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }

    private var index = 0

    @SuppressLint("NotifyDataSetChanged")
    override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {
        val item = list[position]
        holder.bind(item)
        holder.binding.apply {

            root.setOnClickListener {
                index = position
                notifyDataSetChanged()
            }

            radioButton.setOnClickListener {
                index = position
                notifyDataSetChanged()
            }

            radioButton.isChecked = index == position
        }
    }


    fun getAccountChoose() : String{
        return list[index]
    }

    fun getPrice() : String{
        return "80,000,000 VND"
    }

    override fun getItemCount(): Int {
        return list.size
    }
}